import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author shubhamgupta
 */
public class Interactive {
     public static void main(String[] args) throws IOException{
        List<String> li = new ArrayList<String>();
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(in);
        while(true){                                                // for the user inpt for different task
            
            System.out.print("Input: ");
            String[] arg = br.readLine().split(" ",2);              // to read the user input
            if(arg[0].equals("<list>")){
                System.out.println("Output:");
                if(li.size()==0){
                    System.out.println("No task added");
                }
                else{
                for(int i =0;i<li.size();i++){
                    System.out.println((i+1)+". "+li.get(i));       // to print the stored tasks
                }}
                System.out.println();
            }
            else if(arg[0].equals("<add>")){    
                li.add(arg[1]);                                     // add task to arraylist
                System.out.println("Output: Added with id#"+li.size());
                System.out.println();
            }
            else if(arg[0].equals("<done>")){
                System.out.println("Output:");
                if(li.isEmpty()){                                   // to check if list is empty
                   System.out.println("No task to be completed. Add task first");
                }
                else if(li.size()<Integer.parseInt(arg[1])){
                   System.out.println("No such task");
                }  
                else{
                li.remove(Integer.parseInt(arg[1])-1);              // to remove task from arraylist
                if(li.isEmpty()){
                    System.out.println("All task completed. Add new task.");
                }
                else{
                for(int i =0;i<li.size();i++){
                    System.out.println((i+1)+". "+li.get(i));       // print available task after removal from arraylist
                }}
                
            }
                System.out.println();
        }
            else if(arg[0].equals("<exit>")){                       // to exit from the system
                System.out.println("Output: Bye Bye");
                break;
                
            }
            else{
                System.out.println("Output: Wrong Input");          // in case of wrong input by the user
                System.out.println();
            }
    }
  }
}

