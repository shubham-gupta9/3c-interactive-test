# * Project for 3C Interactive Internship: #

## * This a basic to do task application that has the functionalities like: ##
### 1. Add a task using <add> yourtask ###
### 2. List task using <list> ###
### 3. Complete a task using <done> taskId ###
### 4. Exit the system using <exit> ### 


## * Sample Interaction of the Application: ##

* Input: <list>
* Output:
No task added

* Input: <done> 1
* Output:
No task to be completed. Add task first

* Input: <add> Buy Milk
* Output: Added with id#1

* Input: <add> Pick up Dry Cleaners
* Output: Added with id#2

* Input: <list>
* Output:
1. Buy Milk
2. Pick up Dry Cleaners

* Input: <done> 1
* Output:
1. Pick up Dry Cleaners

* Input: <done> 1
* Output:
All task completed. Add new task.

* Input: <exit>
* Output: Bye Bye